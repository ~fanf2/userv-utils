/*
 * Copyright 1996-2013,2016 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
 * Copyright 1999,2003
 *    Chancellor Masters and Scholars of the University of Cambridge
 * Copyright 2010 Tony Finch <fanf@dotat.at>
 * Copyright 2013,2016 Mark Wooding <mdw@distorted.org.uk>
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with userv-utils; if not, see http://www.gnu.org/licenses/.
 */

#ifndef UCGI_H
#define UCGI_H

#include <stdlib.h>

#ifdef DEBUG
#  define D(x) x
#else
#  define D(x)
#endif

#define MAX_ARGS 1024
#define MAX_USERNAME_LEN 1024
#define MAX_SCRIPTPATH_LEN 1024
#define MAX_ENVVAR_NAME 128
#define MAX_ENVVAR_VALUE (1024*1024)
#define MAX_ENVVARS 256

void syserror(const char *m);
void error(const char *m, int st);
void *xmalloc(size_t sz);
void xsetenv(const char *en, const char *ev, int overwrite);
void *xrealloc(void *ptr, size_t sz);

const char **load_filters(unsigned flags, const char *first, ...);
#define LOADF_MUST 1u
#define LF_END ((const char *)0)

void filter_environment(unsigned flags, const char *prefix_in,
			const char *const *patv,
			const char *const *defaults,
			void (*foundone)(const char *fulln, const char *en,
					 const char *ev, void *p),
			void *p);
#define FILTF_WILDCARD 1u

extern int debugmode;

#endif
